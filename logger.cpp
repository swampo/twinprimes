/***************************************************************************
 *   Copyright (C) 2016 by Fabiano Riccardi, Andrea Salmoiraghi            *
 *                                                                         *
 *   This file is part of TwinPrimes.                                      *
 *                                                                         *
 *   TwinPrimes is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   TwinPrimes is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Twin Primes; if not, see <http://www.gnu.org/licenses/>    *
 ***************************************************************************/

#ifndef LOGGER

#define LOGGER logger.log()
#define LOG(x) logger.log(x)
#define INFO 0
#define STDOUT 1

#include <iostream>
#include <fstream>
#include <time.h>
#include <string>

using namespace std;

class Logger {
public:
	Logger(): out(cout.rdbuf()) {}

	Logger(int id1, int id2): out(cout.rdbuf()), onlyStdOut(false) {
		init(id1, id2);
	}

	Logger(int id1, int id2, string file): out(cout.rdbuf()), onlyStdOut(false) {
		init(id1, id2, file);
	}

	Logger(int id1, int id2, streambuf* output): out(output), onlyStdOut(false) {
		init(id1, id2);
	}

	void init(int id1, int id2) {
		idPrimary = id1;
		idSecondary = id2;
		prefix = "";

		char num[3];
		sprintf(num, "%d", id1);
		if (idPrimary == 0) {
			prefix = "[Master]:";
		} else {
			prefix = "[Slave ";
			prefix = prefix + num + "]:";
		}
	}

	void init(int id1, int id2, streambuf* output) {
		init(id1, id2);
		setOutputChannel(output);
	}

	void init(int id1, int id2, string file) {
		init(id1, id2);

		file = file + "_" + to_string(id1) + ".log";
		if (f.is_open()) {
			f.close();
		}
		f.open(file, ios::out);
		if (!f.is_open()) {
			cout << "Error opening file " << file << endl;
			exit(0);
		}
		setOutputChannel(f.rdbuf());
	}

	void setOutputChannel(streambuf* output) {
		out.rdbuf(output);
	}

	void log(const char *s) {
		string aux;
		aux = getPrefix() + s;
		out << aux;
		cout << aux;
	}

	void log(string s) {
		const char *pc = s.c_str();
		log(pc);
	}

	Logger& log() {
		onlyStdOut = false;
		return log(INFO);
	}

	Logger& log(int level) {
		if (level == STDOUT) {
			onlyStdOut = true;
		}
		if (!onlyStdOut) {
			out << getPrefix();
		}
		cout << getPrefix();
		return *this;
	}

	template <typename T>
	Logger& operator<< ( T const& t)
	{
		if (!onlyStdOut) {
			out << t;
		}
		cout << t;
		return *this;
	}
	// Need for endl
	Logger& operator<< ( ostream & (*f)(std::ostream&))
	{
		if (!onlyStdOut) {
			out << f;
		}
		cout << f;
		return *this;
	}

private:
	string getPrefix() {
		time (&rawtime);
		string aux = "[";
		string time = asctime(localtime(&rawtime));
		time.erase(time.size() - 1);
		aux = aux + time + "]" + prefix;
		return aux;
	}

	time_t rawtime;
	struct tm * timeinfo;
	int idPrimary, idSecondary;
	ostream out;
	string prefix;
	ofstream f;

	bool onlyStdOut;
};

Logger logger;

/*
	Logger test!
*/
/*
int main() {
	string s("Hello world\n");
	logger.init(2, 3, cout.rdbuf());
	logger.log(s);

	char filename[10] = "log.txt";
	ofstream f(filename, ios::out);

	logger.setOutputChannel(f.rdbuf());
	logger.log(s);
	logger.log(filename);

	LOG(INFO) << "Test " << 54 << 69.3 << endl;
	LOGGER << "Test2 " << 93.4 << endl;

	f.close();

	logger.setOutputChannel(cerr.rdbuf());
	logger.log(s);
}*/

#endif // LOGGER