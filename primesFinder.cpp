/***************************************************************************
 *   Copyright (C) 2016 by Fabiano Riccardi, Andrea Salmoiraghi            *
 *                                                                         *
 *   This file is part of TwinPrimes.                                      *
 *                                                                         *
 *   TwinPrimes is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   TwinPrimes is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Twin Primes; if not, see <http://www.gnu.org/licenses/>    *
 ***************************************************************************/

#ifndef PRIMES
#define PRIMES

#include <vector>
#include <array>
#include <sstream>
#include <functional>
#include <chrono>
#include <iterator>
#include <cstring>
#include <math.h>
#include <limits>
#include <iomanip>
#include <bitset>
#include <omp.h>

#include "logger.cpp"
#include "rabinMiller.cpp"
#include "randomized.cpp"

using namespace std;

class PrimesFinder {
public:
	PrimesFinder(): idAlgorithm(0), gallery(NULL), galleryInt(NULL), preshooting(NULL), tempPreshooting(NULL) {
		loadFirstPrimes();
	}

	/*
	 * Start to find primes in the gallery with the algorithm choosen. Compute the partial Brun's constant and return it
	 */
	double find() {
		//SOE Cache multiple tests
		if (idAlgorithm == algorithmsList.size() - 2) {
			doFindCacheTests();
		} else if (idAlgorithm == algorithmsList.size() - 1) {
			doFindeCachePreshootingTests();
		} else {
			while (offset < limit) {
				//At the first gallery offset is 0, like false
				doFind(offset);
				offset = offset + sizeGallery;
				if (offset == sizeGallery) {
					offset -= 2; //The gallery next to gallery zero must start from -2 pos to consider also the final 29 of the previous gallery taht can be twins with the first 1 of the current
				}
			}
		}
		return brun;
	}

	/*
	 * Set the offset of the gallery
	 */
	void setOffset(unsigned long long o) {
		if (o > 2) {
			o = o - 2;
		}
		offset = o;
		//Consistent bounds
		if (offset >= limit) {
			setLimit(offset + sizeGallery);
		}
		LOGGER << "Set offset: " << offset << endl;
	}
	/*
	 * Set the upper limit where to stop the search
	 */
	void setLimit(unsigned long long l) {
		limit = l - 2; // To consider the last possible prime of the previous gallery
		//Consistent bounds
		if (offset >= limit) {
			setLimit(offset + sizeGallery);
		}
		LOGGER << "Set limit: " << limit << endl;
	}

	/*
	 * Set the algorithm to use in the research
	 */
	void setAlgorithm(int id) {
		idAlgorithm = id;
		LOGGER << "Set algorithm id: " << idAlgorithm << " [" << algorithmsList[idAlgorithm].name << "]" << endl;
		setOnlyTwins(algorithmsList[idAlgorithm].onlyTwins);
		setCompressed(algorithmsList[idAlgorithm].compressed);
		setPreshooting(algorithmsList[idAlgorithm].preshooting);
	}

	/*
	 * Set the use of preshooting or not
	 */
	void setPreshooting(bool use) {
		usePreshooting = use;
		if (usePreshooting) {
			LOGGER << "Use preshooting: yes" << endl;
		} else {
			LOGGER << "Use preshooting: no" << endl;
		}
		loadPreshooting();
	}

	/*
	 * Set true if want to store the preshooting compresse: 1 bit for each number instead of 1 byte
	 */
	void setCompressed(bool compr) {
		compressed = compr;
		if (compressed) {
			LOGGER << "Compressed preshooting: yes" << endl;
		} else {
			LOGGER << "Compressed preshooting: no" << endl;
		}
		loadPreshooting();
	}

	/*
	 * Set true if want to search only twins primes numbers
	 * NOTE: this works only with basic algorithm!!!
	 */
	void setOnlyTwins(bool only) {
		onlyTwins = only;
		if (onlyTwins) {
			LOGGER << "Search only twins: yes" << endl;
		} else {
			LOGGER << "Search only twins: no" << endl;
		}
		//Preshooting may change, reload
		loadPreshooting();
	}

	/*
	 * Set the use of openMP or not
	 */
	void setOpenMP(char val) {
		ompEnabled = val;
		if (ompEnabled) {
			LOGGER << "Use multithread with openMP: yes" << endl;
		} else {
			LOGGER << "Use multithread with openMP: no" << endl;
		}
	}

	/*
	 * Set the number of threads
	 */
	void setNumOfThreads(int val) {
		numThreads = val;
		LOGGER << "Number of threads with openMP: " << numThreads << endl;
	}

	/*
	 * Return the list of the availables algorithms
	 */
	static string getAvailableAlgorithms() {
		stringstream ss;
		int i = 0;
		for (const AlgFunc_t &a : algorithmsList) {
			ss << i << " - " << a.name << endl;
			i++;
		}
		return ss.str();
	}

	typedef function<void(PrimesFinder&)> funcptr;
	typedef long double size_brun;
	struct AlgFunc_t {
		string name;
		funcptr func;
		bool preshooting;
		bool compressed;
		bool onlyTwins;
	};

	static const vector<AlgFunc_t> algorithmsList;
	//Default gallery size 2*3*5*7*11*13*17*19*23=223092870
	static const unsigned int sizeGallery = 223092870;

private:
	void doFind(bool notGalleryZero) {
		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;

		LOGGER << endl;
		LOGGER << "GALLERY " << (offset + 2) / sizeGallery << endl;
		LOGGER << "Search primes starting at " << offset << endl;

		start = chrono::high_resolution_clock::now();
		// The first gallery 0-223092868 is quite different from the others galleries
		if (notGalleryZero) {
			algorithmsList[idAlgorithm].func(*this);
		} else {
			sieveGalleryZero(); //Dedicated method
		}
		stop = chrono::high_resolution_clock::now();

		//Update total values
		totalPrimes += primes;
		totalTwinPrimes += twinPrimes;
		totalBrun += brun;

		LOGGER << "Gallery Search time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms (Offset " << offset << " - Limit " << offset + sizeGallery << ")" << endl;
		LOGGER << "Gallery Primes found: " << primes << " (Offset " << offset << " - Limit " << offset + sizeGallery << ")" << endl;
		LOGGER << "Gallery Twin primes found: " << twinPrimes << " (Offset " << offset << " - Limit " << offset + sizeGallery << ")" << endl;
		LOGGER << "Gallery Brun's constant: " << std::setprecision(std::numeric_limits<size_brun>::digits10 + 1) << brun << " (Offset " << offset << " - Limit " << offset + sizeGallery << ")" << endl << endl;
	}

	// Launch multiple tests on sieve algorithm using cache
	void doFindCacheTests() {
		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		printInfo = false;
		for (int j = 0; j < 4; j++) {
			setOffset(offsets[j]);
			LOGGER << "Search primes in gallery starting at " << offset << endl;
			//For each subgallery
			for (int i = 0; i < 10; i++) {
				//variabile globale che serve a decidere la dimensione della sottogallery
				sizeSubgallery = sizeSubgalleryVector[i];
				start = chrono::high_resolution_clock::now();
				algorithmsList[idAlgorithm].func(*this);
				stop = chrono::high_resolution_clock::now();
				LOGGER << "Search time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms (with subgallery " << sizeSubgallery << ")" << endl;
			}
			LOGGER << "Primes found: " << primes << " (Offset " << offset << ")" << endl;
			LOGGER << "Twin primes found: " << twinPrimes << " (Offset " << offset << ")" << endl << endl;
		}
	}

	// Launch multiple tests on sieve algorithm using cache and preshooting
	void doFindeCachePreshootingTests() {
		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		printInfo = false;
		for (int j = 0; j < 4; j++) {
			setOffset(offsets[j]);
			LOGGER << "Search primes in gallery starting at " << offset << endl;
			//For each subgallery
			for (int i = 0; i < 14; i++) {
				//variabile globale che serve a decidere la dimensione della sottogallery
				sizeSubgallery = sizeSubgalleryVector2[i];
				start = chrono::high_resolution_clock::now();
				algorithmsList[idAlgorithm].func(*this);
				stop = chrono::high_resolution_clock::now();
				LOGGER << "Search time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms (with subgallery " << sizeSubgallery << ")" << endl;
			}
			LOGGER << "Primes found: " << primes << " (Offset " << offset << ")" << endl;
			LOGGER << "Twin primes found: " << twinPrimes << " (Offset " << offset << ")" << endl << endl;
		}
	}

//******************************************//
//	BASIC PRIMALITY TEST                    //
//******************************************//
	void basic() {
		//Potential primes in modulo 30 are 1,7,11,13,17,19,23,29
		// Gallery is multiple of 30, starting from the position 28 (to consider the last 29 that could be twin primes with the next 1)
		// 1-->3, 7-->9,...,29-->1
		int size;
		vector<int> pPrimes;
		if (onlyTwins) {
			// Check onlyt potential twins primes
			pPrimes.push_back(1);
			pPrimes.push_back(13);
			pPrimes.push_back(19);
			size = sizeGalleryMod30_3;
		} else {
			pPrimes.push_back(1);
			pPrimes.push_back(3);
			pPrimes.push_back(9);
			pPrimes.push_back(13);
			pPrimes.push_back(15);
			pPrimes.push_back(19);
			pPrimes.push_back(21);
			pPrimes.push_back(25);
			size = sizeGalleryMod30_8;
		}
		basicAlgorithm(pPrimes, size);
	}

	/*
	 *	Basic primality test using trial divisions
	 */
	template<typename T>
	void basicAlgorithm(vector<T> &pPrimes, int size) {
		primes = 0;
		twinPrimes = 0;
		int start = 0;
		int startTwin = 0;
		if (usePreshooting) {
			// Start from 29, that is the 9° prime in the list of firstPrimes
			start = 9;
			startTwin = 9;
		}
		if (onlyTwins) {
			// If consider only twins, preshooting doesn't shoot out primes multiple of 2,3,5,7,11,13,17,19,23
			// in positions 1,13,19. So check also if divisible by this primes
			startTwin = 0;
		}
		unsigned long long p;
		int k = 0;
		int ompPrimes = 0, ompTwinPrimes = 0;
		#pragma omp parallel for private(p) reduction(+:ompPrimes,ompTwinPrimes,k) num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < size; i++) {
			//If not eliminated in preshooting (Not multiple of 7,11,13,17,19,23)
			if (!usePreshooting || preshooting[i]) {
				p = offset + (i / pPrimes.size()) * 30 + pPrimes[i % pPrimes.size()];
				//Check is working
				if (k++ == 2000000) {
					k = 0;
					LOG(STDOUT) << "I'm still working. I'm on= " << p <<  " over " << offset + sizeGallery << endl;
				}
				if (isPrime(p, start)) {
					ompPrimes++;
					if ((onlyTwins || !usePreshooting || preshooting[i + 1]) && (onlyTwins || potentialTwin(i % pPrimes.size())) && isPrime(p + 2, startTwin)) {
						ompTwinPrimes++;
					}
				}
			}
		}
		primes = ompPrimes;
		twinPrimes = ompTwinPrimes;
	}

	/*
	 * Given the modulo 30 of a number, return true if it is in a potential twin prime position (11,17,29)
	 * in an array as (29,1,7,11,13,17,19,23)
	 */
	bool potentialTwin(int r) {
		if (r == 0 || r == 3 || r == 5) {
			return true;
		}
		return false;
	}

	/*
	 * Primality test with trivial division
	 * Check a given number, return true if prime
	 */
	bool isPrime(unsigned long long number) {
		return isPrime(number, 0);
	}
	// Start to check from the start-ith prime
	bool isPrime(unsigned long long number, int start) {
		unsigned int limit = sqrt((double)number);
		for (int i = start; firstPrimes[i] <= limit; i++) {
			if (number % firstPrimes[i] == 0) {
				return false;
			}
		}
		return true;
	}

//******************************************//
//	END BASIC PRIMALITY TEST                //
//******************************************//

//******************************************//
//	RANDOMIZED PRIMALITY TEST               //
//******************************************//

	void randomized() {
		int size;
		vector<int> pPrimes;
		if (onlyTwins) {
			// Check onlyt potential twins primes
			pPrimes.push_back(1);
			pPrimes.push_back(13);
			pPrimes.push_back(19);
			size = sizeGalleryMod30_3;
		} else {
			pPrimes.push_back(1);
			pPrimes.push_back(3);
			pPrimes.push_back(9);
			pPrimes.push_back(13);
			pPrimes.push_back(15);
			pPrimes.push_back(19);
			pPrimes.push_back(21);
			pPrimes.push_back(25);
			size = sizeGalleryMod30_8;
		}
		doRandomized(pPrimes, size);
	}

	/*
	 *	Basic primality test using trial divisions
	 */
	template<typename T>
	void doRandomized(vector<T> &pPrimes, int size) {
		primes = 0;
		twinPrimes = 0;
		unsigned long long p;
		int k = 0;
		int ompPrimes = 0, ompTwinPrimes = 0;
		#pragma omp parallel for private(p) reduction(+:ompPrimes,ompTwinPrimes,k) num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < size; i++) {
			//If not eliminated in preshooting (Not multiple of 7,11,13,17,19,23)
			if (!usePreshooting || preshooting[i]) {
				p = offset + (i / pPrimes.size()) * 30 + pPrimes[i % pPrimes.size()];
				//Check is working
				if (k++ == 2000000) {
					k = 0;
					LOG(STDOUT) << "I'm still working. I'm on= " << p <<  " over " << offset + sizeGallery << endl;
				}
				if (randIsPrime(p, 1) && isPrime(p)) {
					ompPrimes++;
					if ((onlyTwins || !usePreshooting || preshooting[i + 1]) && (onlyTwins || potentialTwin(i % pPrimes.size())) && randIsPrime(p + 2, 1) && isPrime(p + 2)) {
						ompTwinPrimes++;
					}
				}
			}
		}
		primes = ompPrimes;
		twinPrimes = ompTwinPrimes;
	}

//******************************************//
//	END RANDOMIZED                          //
//******************************************//

//******************************************//
//	RABIN MILLER PRIMALITY TEST             //
//******************************************//

	void rabinMiller() {
		int size;
		vector<int> pPrimes;
		if (onlyTwins) {
			// Check onlyt potential twins primes
			pPrimes.push_back(1);
			pPrimes.push_back(13);
			pPrimes.push_back(19);
			size = sizeGalleryMod30_3;
		} else {
			pPrimes.push_back(1);
			pPrimes.push_back(3);
			pPrimes.push_back(9);
			pPrimes.push_back(13);
			pPrimes.push_back(15);
			pPrimes.push_back(19);
			pPrimes.push_back(21);
			pPrimes.push_back(25);
			size = sizeGalleryMod30_8;
		}
		doRabinMiller(pPrimes, size);
	}

	/*
	 *	Basic primality test using trial divisions
	 */
	template<typename T>
	void doRabinMiller(vector<T> &pPrimes, int size) {
		primes = 0;
		twinPrimes = 0;
		unsigned long long p;
		int k = 0;
		int ompPrimes = 0, ompTwinPrimes = 0;
		#pragma omp parallel for private(p) reduction(+:ompPrimes,ompTwinPrimes,k) num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < size; i++) {
			//If not eliminated in preshooting (Not multiple of 7,11,13,17,19,23)
			if (!usePreshooting || preshooting[i]) {
				p = offset + (i / pPrimes.size()) * 30 + pPrimes[i % pPrimes.size()];
				//Check is working
				if (k++ == 2000000) {
					k = 0;
					LOG(STDOUT) << "I'm still working. I'm on= " << p <<  " over " << offset + sizeGallery << endl;
				}
				if (rm_is_prime(p)) {
					ompPrimes++;
					if ((onlyTwins || !usePreshooting || preshooting[i + 1]) && (onlyTwins || potentialTwin(i % pPrimes.size())) && rm_is_prime(p + 2)) {
						ompTwinPrimes++;
					}
				}
			}
		}
		primes = ompPrimes;
		twinPrimes = ompTwinPrimes;
	}

//******************************************//
//	END RABIN MILLER                        //
//******************************************//

//******************************************//
//	SIEVE OF ERATOSTHENES                   //
//******************************************//
	// Sieve not optimezed using gallery memorized in char
	void sieve() {
		sieveAlgorithm(gallery, sizeGalleryBit, false);
	}
	// Sieve not optimezed using gallery memorized in int
	void sieveInt() {
		sieveAlgorithm(galleryInt, sizeGalleryBitInt, false);
	}
	// Sieve optimezed using gallery memorized in char
	void sieveOpt() {
		sieveAlgorithm(gallery, sizeGalleryBit, true);
	}
	// Sieve optimezed using gallery memorized in int
	void sieveIntOpt() {
		sieveAlgorithm(galleryInt, sizeGalleryBitInt, true);
	}

	/*
	 * Sieve of Eratosthenes algorithm
	 * Not optimized: require memory to store each number in the gallery
	 */
	template<typename T>
	void sieveAlgorithm(T *gall, int size, bool optimized) {
		int start = 0;
		int val = 0;
		int factor = 1;
		if (optimized) {
			//Compute the initial value to taint all multiple of 2
			for (int i = 0; i < sizeof(T); i++) {
				val = val << 8;
				val += 85; //85 is 01010101, taint all even numbers in the byte
			}
			start = 1; // Start to check from prime 3
			factor = 2; // Not consider increments to even numbers
		}
		initializeArray(&gall, size, val);
		//Cycle for each number lesser that the sqrt(offset+size) and set to 1 the multiples in the gallery
		int limit = sqrt(offset + sizeGallery);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		#pragma omp parallel for num_threads(numThreads) if(ompEnabled)
		for (int i = start; i < max_i; i++) {
			// Temp is the position corresponding to the first multiple of i greater than offset
			// Taint the multiples of i in gallery
			for (int temp = findFirstTaint(firstPrimes[i], optimized); temp < sizeGallery; temp += firstPrimes[i] * factor) {
				gall[temp / (sizeof(T) * 8)] |= 1 << temp % (sizeof(T) * 8);
			}
		}
		countPrimes(&gall);
	}

	/*
	 * SoE dedicated to the firstGallery 0-223092868 (NOT OPTIMIZED)
	 */
	void sieveGalleryZero() {
		initializeArray(&gallery, sizeGalleryBit);
		gallery[0] |= 1;
		gallery[0] |= (1 << 1);
		unsigned long long temp;
		//Cycle for each number lesser that the sqrt(offset+size)
		int limit = sqrt(offset + sizeGallery - 2);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		#pragma omp parallel for num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < max_i; i++) {
			for (int temp = firstPrimes[i] * 2; temp < sizeGallery; temp += firstPrimes[i]) {
				gallery[temp / (sizeof(char) * 8)] |= 1 << temp % (sizeof(char) * 8);
			}
		}
		countPrimes(&gallery);
		primes++; //Consider the 2: is prime...
	}

	/*
	 * SoE with cache optimization
	 * Divide the gallery in subgallery and apply the SoE on each subgallery
	 * In this way the number analyzed should be stay in cache more time, reducing the MISS
	 */
	void sieveCache() {
		initializeArray(&galleryInt, sizeGalleryBitInt);
		int limit = sqrt(offset + sizeGallery);
		int mem_syze = 100000; //~64kB=65536byte --> 65536*8=524288 bit=numbers
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		//Cycle each subgallery
		#pragma omp parallel for collapse(2) num_threads(numThreads) if(ompEnabled)
		for (int j = 0; j < 279; j++) {
			for (int i = 0; i < max_i; i++) {
				for (int temp = findFirstTaint(firstPrimes[i], offset + j * 800000) + j * 800000; temp < (j + 1) * 800000 && temp < sizeGallery; temp += firstPrimes[i]) {
					galleryInt[temp / (sizeof(int) * 8)] |= 1 << temp % (sizeof(int) * 8);
				}
			}
		}
		countPrimes(&galleryInt);
	}
	// With the parameter sizeSubgallery to test more values. Used in SoE multiple tests with cache
	void sieveCache2() {
		initializeArray(&galleryInt, sizeGalleryBitInt);
		double aux = offset + sizeGallery;
		int limit = sqrt(aux);
		int memSize = sizeSubgallery;
		int memSizeBit = memSize * 8;
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		#pragma omp parallel for collapse(2) num_threads(numThreads) if(ompEnabled)
		for (int j = 0; j < sizeGallery / memSizeBit + 1; j++) {
			for (int i = 0; i < max_i; i++) {
				for (int temp = findFirstTaint(firstPrimes[i], offset + j * memSizeBit) + j * memSizeBit; temp < (j + 1) * memSizeBit && temp < sizeGallery; temp += firstPrimes[i]) {
					galleryInt[temp / (sizeof(int) * 8)] |= 1 << temp % (sizeof(int) * 8);
				}
			}
		}
		countPrimes(&galleryInt);
	}

	/*
	 * SoE with preshooting optimization
	 */
	void sievePre() {
		if (compressed) {
			return sievePreComp(); // Preshooting compressed
		}
		return sievePreNotComp(); // Preshooting not compressed
	}
	//SoE with preshooting not compressed
	void sievePreNotComp() {
		//Copy the preshooting already loaded in a temp where will be taint the numbers not prime
		memcpy(tempPreshooting, preshooting, sizeGalleryMod30_8);
		//Potential primes in modulo 30 are 1,7,11,13,17,19,23,29
		// Gallery is multiple of 30, starting from the position 28 (to consider the last 29 that could be twin primes with the next 1)
		// 1-->3, 7-->9,...,29-->1
		vector<int> pPrimes;
		pPrimes.push_back(1);
		pPrimes.push_back(3);
		pPrimes.push_back(9);
		pPrimes.push_back(13);
		pPrimes.push_back(15);
		pPrimes.push_back(19);
		pPrimes.push_back(21);
		pPrimes.push_back(25);

		int limit = sqrt(offset + sizeGallery);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		start = chrono::high_resolution_clock::now();

		#pragma omp parallel for schedule(dynamic, 500) collapse(2) num_threads(numThreads) if(ompEnabled)
		for (int i = 9; i < max_i; i++) {
			for (int j = 0; j < pPrimes.size(); j++) {
				for (int block = findFirstTaintOddRest(firstPrimes[i], pPrimes[j]); block < sizeGallery / 30; block += firstPrimes[i]) {
					int pos = block * 8;
					tempPreshooting[pos + j] = 0;
				}
			}
		}
		stop = chrono::high_resolution_clock::now();
		LOGGER << "Taint time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

		countPrimesPre(&tempPreshooting);
	}
	// SoE with preshooting compressed
	void sievePreComp() {
		//Copy the preshooting already loaded in a temp where will be taint the numbers not prime
		memcpy(tempPreshooting, preshooting, sizeGalleryMod30_8 / 8);
		//Potential primes in modulo 30 are 1,7,11,13,17,19,23,29
		// Gallery is multiple of 30, starting from the position 28 (to consider the last 29 that could be twin primes with the next 1)
		// 1-->3, 7-->9,...,29-->1
		vector<int> pPrimes;
		pPrimes.push_back(1);
		pPrimes.push_back(3);
		pPrimes.push_back(9);
		pPrimes.push_back(13);
		pPrimes.push_back(15);
		pPrimes.push_back(19);
		pPrimes.push_back(21);
		pPrimes.push_back(25);

		int limit = sqrt(offset + sizeGallery);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		start = chrono::high_resolution_clock::now();

		#pragma omp parallel for schedule(dynamic, 500) collapse(2) num_threads(numThreads) if(ompEnabled)
		for (int i = 9; i <= max_i; i++) {
			for (int j = 0; j < pPrimes.size(); j++) {
				for (int block = findFirstTaintOddRest(firstPrimes[i], pPrimes[j]); block * 30 < sizeGallery; block += firstPrimes[i]) {
					int pos = block;
					tempPreshooting[pos] &=  ~(1 << j);
				}
			}
		}

		stop = chrono::high_resolution_clock::now();
		LOGGER << "Taint time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

		countPrimesPreCom(&tempPreshooting);
	}
	//SoE with preshooting not compressed using cache for multiple tests with different values of subgallery
	void sievePreCache() {
		//Copy the preshooting already loaded in a temp where will be taint the numbers not prime
		memcpy(tempPreshooting, preshooting, sizeGalleryMod30_8);

		double aux = offset + sizeGallery;
		int limit = sqrt(aux);
		int memSize = sizeSubgallery;
		int memSizeBit = memSize * 8;
		//memSizeBit must be multiple of 30
		memSizeBit /= 30;
		memSizeBit *= 30;

		//Potential primes in modulo 30 are 1,7,11,13,17,19,23,29
		// Gallery is multiple of 30, starting from the position 28 (to consider the last 29 that could be twin primes with the next 1)
		// 1-->3, 7-->9,...,29-->1
		vector<int> pPrimes;
		pPrimes.push_back(1);
		pPrimes.push_back(3);
		pPrimes.push_back(9);
		pPrimes.push_back(13);
		pPrimes.push_back(15);
		pPrimes.push_back(19);
		pPrimes.push_back(21);
		pPrimes.push_back(25);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		start = chrono::high_resolution_clock::now();

		for (int j = 0; j < sizeGallery / memSizeBit + 1; j++) {
			#pragma omp parallel for schedule(dynamic, 500) collapse(2) num_threads(numThreads) if(ompEnabled)
			for (int i = 9; i < max_i; i++) {
				for (int ii = 0; ii < pPrimes.size(); ii++) {
					int start = findFirstTaintOddRest(firstPrimes[i], pPrimes[ii], offset + j * memSizeBit);
					int block = (start * 30 + j * memSizeBit) / 30;
					for (; block * 30 < (j + 1) * memSizeBit && block * 30 < sizeGallery; block += firstPrimes[i]) {
						int pos = block * 8;
						tempPreshooting[pos + ii] = 0;
					}
				}
			}
		}

		stop = chrono::high_resolution_clock::now();
		LOGGER << "Taint time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

		countPrimesPre(&tempPreshooting);
	}
	//SoE with preshooting not compressed using cache with the best value for RPI find from the multiple tests
	void sievePreCacheFinal() {
		//Copy the preshooting already loaded in a temp where will be taint the numbers not prime
		memcpy(tempPreshooting, preshooting, sizeGalleryMod30_8);

		double aux = offset + sizeGallery;
		int limit = sqrt(aux);
		int memSize = 30000000;
		int memSizeBit = memSize * 8;
		//memSizeBit must be multiple of 30
		memSizeBit /= 30;
		memSizeBit *= 30;

		//Potential primes in modulo 30 are 1,7,11,13,17,19,23,29
		// Gallery is multiple of 30, starting from the position 28 (to consider the last 29 that could be twin primes with the next 1)
		// 1-->3, 7-->9,...,29-->1
		vector<int> pPrimes;
		pPrimes.push_back(1);
		pPrimes.push_back(3);
		pPrimes.push_back(9);
		pPrimes.push_back(13);
		pPrimes.push_back(15);
		pPrimes.push_back(19);
		pPrimes.push_back(21);
		pPrimes.push_back(25);
		// Search could be optimized
		int max_i = 9;
		while (firstPrimes[max_i++] <= limit);

		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;
		start = chrono::high_resolution_clock::now();

		for (int j = 0; j < sizeGallery / memSizeBit + 1; j++) {
			#pragma omp parallel for schedule(dynamic, 500) collapse(2) num_threads(numThreads) if(ompEnabled)
			for (int i = 9; i < max_i; i++) {
				for (int ii = 0; ii < pPrimes.size(); ii++) {
					int start = findFirstTaintOddRest(firstPrimes[i], pPrimes[ii], offset + j * memSizeBit);
					int block = (start * 30 + j * memSizeBit) / 30;
					for (; block * 30 < (j + 1) * memSizeBit && block * 30 < sizeGallery; block += firstPrimes[i]) {
						int pos = block * 8;
						tempPreshooting[pos + ii] = 0;
					}
				}
			}
		}
		stop = chrono::high_resolution_clock::now();
		LOGGER << "Taint time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

		countPrimesPre(&tempPreshooting);
	}
//******************************************//
//	END SIEVE OF ERATOSTHENES               //
//******************************************//

//******************************************//
//	SIEVE OF ERATOSTHENES  UTILS            //
//******************************************//
	/*
	 * Try to print the info at the number desired, like 1000000, 2000000...
	 * depending on grainInfo.
	 * NOTE: not work properly, used only for testing in single execution (not with MPI)
	 * Further, take time to print the info. Disable if not necessary
	 */
	void printCurrentStatus(unsigned long long currentNum) {
		if (printInfo && currentNum > lastGalleryInfo * grainInfo) {
			currentNum = lastGalleryInfo * grainInfo;
			lastGalleryInfo++;
			LOGGER << "Primes found: " << totalPrimes + primes << " (Limit " << currentNum << ")" << endl;
			LOGGER << "Twin primes found: " << totalTwinPrimes  + twinPrimes << " (Limit " << currentNum << ")" << endl;
			LOGGER << "Brun's constant: " << std::setprecision(std::numeric_limits<size_brun>::digits10 + 1) << totalBrun + brun << " (Limit " << currentNum << ")" << endl;
			// getchar();
		}
	}

	/*
	 *	Load the prime numbers less than 10^8 from files
	 */
	void loadFirstPrimes() {
		firstPrimes.reserve(5761455);
		string base = "primes/primes", suffix = ".txt";
		char num[2];
		for (int i = 1; i <= 6; i++) {
			stringstream ss;
			ss << base << i << suffix;
			const string& filename = ss.str();
			LOGGER << "Loading " << filename << "..." << endl;
			ifstream file(filename.c_str(), ios::in);
			if (!file.is_open()) {
				LOGGER << "Error during opening of file: " << filename << endl;
				exit(0);
			}
			string str;
			while (getline(file, str)) {
				int prime = atoi(str.c_str());
				if (prime < 100000000) {
					firstPrimes.push_back(prime);
				}
			}
			file.close();
		}
		LOGGER << "First primes loaded correctly" << endl;
	}

	/*
	 * Initialize gallery to 0 to set all to potential primes
	 */
	template <typename T>
	void initializeArray(T **arr, int size) {
		initializeArray(arr, size, 0);
	}
	template <typename T>
	void initializeArray(T **arr, int size, int val) {
		if (!*arr) {
			*arr = (T*) malloc(size * sizeof(T));
		}
		#pragma omp parallel for num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < size; i++) {
			(*arr)[i] = val;
		}
	}

	/*
	 * Count primes and twin primes
	 * Used with base SoE
	 */
	template <typename T>
	void countPrimes(T **gallery) {
		bool possibleTwin = false;
		primes = 0;
		twinPrimes = 0;
		brun = 0;

		for (int i = 1, q = 0, r = 1; i < sizeGallery; i += 2) {
			if (!((*gallery)[q] & (1 << r))) {
				// printCurrentStatus(offset + i);
				primes++;
				if (possibleTwin) {
					twinPrimes++;
					brun += 1 / (size_brun)(offset + i) + 1 / (size_brun)(offset + i - 2);
				}
				possibleTwin = true;
			} else {
				possibleTwin = false;
			}
			r += 2;
			if (r >= 8 * sizeof(T)) {
				r = r - 8 * sizeof(T);
				q++;
			}
		}
	}

	/*
	 * Count primes and twin primes
	 * Used with SoE with preshooting compressed
	 */
	template <typename T>
	void countPrimesPreCom(T **gallery) {
		primes = 0;
		twinPrimes = 0;
		brun = 0;
		unsigned long long num;

		for (int i = 0, b = 0, r = 0; i < sizeGalleryMod30_8; i++) {
			if ((*gallery)[b] & (1 << r)) {
				// printCurrentStatus(numFromMod8(b, r));
				primes++;
				if (potentialTwin(r)) {
					if ((*gallery)[b] & (1 << (r + 1))) {
						twinPrimes++;
						num = numFromMod8(b, r); // Retrieve the number from the position in module 30
						brun += 1 / (size_brun)num + 1 / (size_brun)(num + 2);
					}
				}
			}
			r++;
			if (r == 8) {
				r = 0;
				b++;
			}
		}
	}

	/*
	 * Count primes and twin primes
	 * Used with preshooting not compressed
	 */
	template <typename T>
	void countPrimesPre(T **gallery) {
		primes = 0;
		twinPrimes = 0;
		brun = 0;
		unsigned long long num;
		int n, r;
		int ompPrimes = 0, ompTwinPrimes = 0;
		size_brun ompBrun = 0;

		#pragma omp parallel for private(num,n,r) reduction(+:ompPrimes,ompTwinPrimes,ompBrun) num_threads(numThreads) if(ompEnabled)
		for (int i = 0; i < sizeGalleryMod30_8; i++) {
			if ((*gallery)[i]) {
				//printCurrentStatus(numFromMod8(i / 8, i % 8));
				ompPrimes++;
				n = i / 8;
				r = i - n * 8;
				if (potentialTwin(r)) {
					if ((*gallery)[i + 1]) {
						ompTwinPrimes++;
						num = numFromMod8(n, r); // Retrieve the number from the position in module 30
						ompBrun += 1 / (size_brun)num + 1 / (size_brun)(num + 2);
					}
				}
			}
		}

		primes = ompPrimes;
		twinPrimes = ompTwinPrimes;
		brun = ompBrun;
	}

	/*
	 * Retrieve the real number given the position in module 30 and the block in the gallery
	 */
	unsigned long long numFromMod8(int block, int pos) {
		int pPrimes[] = {1, 3, 9, 13, 15, 19, 21, 25};
		unsigned long long num = offset + block * 30 + pPrimes[pos];
		return num;
	}

	/*
	 * Given a prime, find the position in the gallery corresponding to the first multiple of prime greater than offset
	 */
	int findFirstTaint(int prime) {
		return findFirstTaint(prime, offset);
	}
	int findFirstTaint(int prime, bool optimized) {
		if (optimized) {
			return findFirstTaintOdd(prime);
		}
		return findFirstTaint(prime);
	}
	//Not optimezed: could return even numbers
	int findFirstTaint(int prime, unsigned long long off) {
		int r = off % prime;
		if (r == 0) {
			return 0;
		} else {
			return prime - r;
		}
	}
	//Optimezed: return only odd multiple of prime
	int findFirstTaintOdd(int prime) {
		int r = offset % prime;
		if (r % 2 == 0) {
			return prime - r;
		} else {
			return prime - r + prime;
		}
	}
	/*
	 * Used with preshooting
	 * Return the position of the first odd multiple of prime that in modulo 30 has the rest given
	 */
	int findFirstTaintOddRest(int prime, int rest) {
		return findFirstTaintOddRest(prime, rest, offset);
	}
	// Start to find wrt the given offset
	int findFirstTaintOddRest(int prime, int rest, unsigned long long off ) {
		// First position of multiple in the gallery
		int r = off % prime;
		int p = prime - r;
		// Rest of position in modulo 30
		r = p % 30;
		int rbase = prime % 30;
		while (r != rest) {
			p += prime;
			r += rbase;
			r = r % 30;
		}
		// Return the number of blocks (size 30) wrt the initial offset
		return p / 30;
	}
//******************************************//
//	END SIEVE OF ERATOSTHENES  UTILS        //
//******************************************//

//******************************************//
//	INITIALIZATION                          //
//******************************************//
	/*
	 * Create the preshooting pattern for primes 2,3,5,7,11,13,17,19,23
	 */
	void loadPreshooting() {
		if (!usePreshooting) {
			return;
		}
		free(preshooting);
		free(tempPreshooting);

		LOGGER << "Preshooting..." << endl;
		// NOTE: the gallery start from offset - 2 to consider the previous potential prime at pos 29.
		// So the first position of preshooting must refer to this prime
		// Start i from the second position
		int size;
		vector<int> pPrimes;
		if (onlyTwins) {
			pPrimes.push_back(11);
			pPrimes.push_back(17);
			pPrimes.push_back(29);
			size = sizeGalleryMod30_3;
		} else {
			pPrimes.push_back(1);
			pPrimes.push_back(7);
			pPrimes.push_back(11);
			pPrimes.push_back(13);
			pPrimes.push_back(17);
			pPrimes.push_back(19);
			pPrimes.push_back(23);
			pPrimes.push_back(29);
			size = sizeGalleryMod30_8;
		}

		if (compressed) {
			size /= 8;
		}

		chrono::high_resolution_clock::time_point start;
		chrono::high_resolution_clock::time_point stop;

		start = chrono::high_resolution_clock::now();
		//Initialize all to 0
		initializeArray(&preshooting, size);
		initializeArray(&tempPreshooting, size);
		int base = 0;

		if (!compressed) {
			#pragma omp parallel for num_threads(numThreads) if(ompEnabled)
			for (int k = 0; k < pPrimes.size(); k++) {
				int i = 1 + k; // Position in preshooting
				for (int n = pPrimes[k]; n < sizeGallery; n += 30) {
					if (!(n % 7 == 0 || n % 11 == 0 || n % 13 == 0 || n % 17 == 0 || n % 19 == 0 || n % 23 == 0)) {
						preshooting[i] = 1;
					}
					i += pPrimes.size();
				}
			}
			preshooting[0] = 1; //The last 29 of the previous gallery
		} else {
			#pragma omp parallel for num_threads(numThreads) if(ompEnabled)
			for (int k = 0; k < pPrimes.size(); k++) {
				int i = 0;
				int r = 1 + k; // Position in preshooting
				if (r == 8) {
					r = 0;
					i++;
				}
				for (int n = pPrimes[k]; n < sizeGallery; n += 30) {
					if (!(n % 7 == 0 || n % 11 == 0 || n % 13 == 0 || n % 17 == 0 || n % 19 == 0 || n % 23 == 0)) {
						preshooting[i] |= 1 << r;
					}
					r += pPrimes.size();
					if (r >= 8) {
						r = r - 8;
						i++;
					}
				}
			}
			preshooting[0] |= 1; //The last 29 of the previous gallery
		}

		stop = chrono::high_resolution_clock::now();
		LOGGER << "Preshooting executed correctly. Time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;
	}

	static const unsigned int sizeGalleryBitInt = (double)sizeGallery / 8 / 4 + 0.5; //6971653;
	static const unsigned int sizeGalleryBit = (double)sizeGallery / 8 + 0.5; //27886609;
	static const unsigned int sizeGalleryMod30_8 = (double)sizeGallery / 30 * 8; //59491432;
	static const unsigned int sizeGalleryMod30_3 = (double)sizeGallery / 30 * 3; //22309287

	int idAlgorithm;

	vector<int> firstPrimes;

	char *gallery;
	unsigned int *galleryInt;
	char *preshooting;
	char *tempPreshooting;

	bool usePreshooting = false;
	bool compressed = false;
	bool onlyTwins = false; // Only for basic
	bool ompEnabled = false; //OpenMP

	int numThreads = 4;

	//Counter
	int primes = 0, twinPrimes = 0, totalPrimes = 0, totalTwinPrimes = 0;
	unsigned long long offset = 0;
	unsigned long long limit = 0;
	size_brun brun = 0, totalBrun = 0;

	//Print Information
	int grainInfo = 100000000;
	int lastGalleryInfo = 1;
	bool printInfo = true;

	//SOE Cache multiple tests with subgallery
	static const int sizeSubgalleryVector[];
	static const int sizeSubgalleryVector2[];
	static const unsigned long long offsets[];
	int sizeSubgallery;
};

const vector<PrimesFinder::AlgFunc_t> PrimesFinder::algorithmsList ({
	{"Basic Primality Test with trial division", &PrimesFinder::basic, false, false, false},
	{"Basic PT using preshooting", &PrimesFinder::basic, true, false, false},
	{"Basic PT searching only twins", &PrimesFinder::basic, false, false, true},
	{"Basic PT with preshooting + only twins", &PrimesFinder::basic, true, false, true},
	{"Randomized Primality Test", &PrimesFinder::randomized, false, false, false},
	{"Randomized PT using preshooting", &PrimesFinder::randomized, true, false, false},
	{"Randomized PT searching only twins", &PrimesFinder::randomized, false, false, true},
	{"Randomized PT with preshooting + only twins", &PrimesFinder::randomized, true, false, true},
	{"Rabin Miller Primality Test", &PrimesFinder::rabinMiller, false, false, false},
	{"Rabin Miller PT using preshooting", &PrimesFinder::rabinMiller, true, false, false},
	{"Rabin Miller PT searching only twins", &PrimesFinder::rabinMiller, false, false, true},
	{"Rabin Miller PT with preshooting + only twins", &PrimesFinder::rabinMiller, true, false, true},
	{"Sieve of Eratosthenes using char to store values (Not optimized)", &PrimesFinder::sieve, false, false, false},
	{"Sieve of Eratosthenes using int to store values (Not optimized)", &PrimesFinder::sieveInt, false, false, false},
	{"SoE (char) optimized: even numbers eliminated during initialization)", &PrimesFinder::sieveOpt, false, false, false},
	{"SoE (int) optimized: even numbers eliminated during initialization", &PrimesFinder::sieveIntOpt, false, false, false},
	{"SoE cache: try to exploit the cache considering subgalleries", &PrimesFinder::sieveCache, false, false, false},
	{"SoE using preshooting (array of char)", &PrimesFinder::sievePre, true, false, false},
	{"SoE using preshooting compressed (array of bit)", &PrimesFinder::sievePre, true, true, false},
	{"SoE cache using preshooting", &PrimesFinder::sievePreCacheFinal, true, false, false},
	{"TEST SoE cache multiple tests to find best subgallery value", &PrimesFinder::sieveCache2},
	{"TEST SoE cache using preshooting multiple tests to find best subgallery value", &PrimesFinder::sievePreCache, true, false, false}
});

//Cache tests values
const int PrimesFinder::sizeSubgalleryVector[] = {50000, 75000, 100000, 200000, 300000, 400000, 500000, 600000, 1000000, 2000000};
const int PrimesFinder::sizeSubgalleryVector2[] = {5000000, 7000000, 10000000, 12000000, 15000000, 17000000, 20000000, 25000000, 28000000, 30000000, 32000000, 35000000, 40000000, 50000000};
const unsigned long long PrimesFinder::offsets[] = {223092870, 2230928700, 223092870000, 22309287000000, 2230928700000000};

//TEST
// int main(int argc, char *argv[]) {
// 	if (argc != 6) {
// 		cout << "Usage: mpi_primes <omp_enabled> <omp_nubmer_of_thread> <id_algorithm> <offset> <limit>" << endl;
// 		cout << "<omp_enabled>: 1 use multithread with openMP, 0 no" << endl;
// 		cout << "<omp_numer_of_thread>: the number of threads to use" << endl;
// 		cout << "<id_algorithm>: the id of the method choosen" << endl;
// 		cout << "<offset>: the number of gallery as the initial offset from which start the search" << endl;
// 		cout << "<limit>: the number of gallery as the upper limit where stop the search" << endl;
// 		cout << endl;
// 		cout << "Implemented algorithms:" << endl;
// 		cout << PrimesFinder::getAvailableAlgorithms() << endl;
// 		cout << endl;
// 		exit(0);
// 	}
// 	//Initialization
// 	string file = "primesFinder";
// 	logger.init(0, 0, file);

// 	unsigned long long offset = (unsigned long long) atoi(argv[4]) * PrimesFinder::sizeGallery;
// 	unsigned long long limit = (unsigned long long) atoi(argv[5]) * PrimesFinder::sizeGallery;

// 	PrimesFinder p;
// 	p.setOpenMP(atoi(argv[1]));
// 	p.setNumOfThreads(atoi(argv[2]));
// 	p.setAlgorithm(atoi(argv[3]));
// 	p.setLimit(limit);
// 	p.setOffset(offset);

// 	p.find();
// }
#endif
