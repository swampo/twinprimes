/***************************************************************************
 *   Copyright (C) 2016 by Fabiano Riccardi, Andrea Salmoiraghi            *
 *                                                                         *
 *   This file is part of TwinPrimes.                                      *
 *                                                                         *
 *   TwinPrimes is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   TwinPrimes is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Twin Primes; if not, see <http://www.gnu.org/licenses/>    *
 ***************************************************************************/

#include "mpi.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <bitset>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <limits>
#include <iomanip>
#include <unistd.h>
#include "logger.cpp"
#include "primesFinder.cpp"

using namespace std;

typedef double size_brun;
size_brun brun;

void updateBrun(size_brun res) {
	brun += res;
	LOGGER << "Partial Brun's constant updated: " << std::setprecision(std::numeric_limits<size_brun>::digits10 + 1) << brun << endl;
}

int main(int argc, char *argv[]) {
	int idAlg;
	//Variabili necessarie ad MPI e per le statistiche
	int nCPU = 1, nTask, myid, numprocs, namelen, destination, initialGallery, lastGallery;
	double startwtime = 0.0, endwtime;
	long numberOfTask = 0;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;

	if (argc != 6) {
		cout << "Usage: mpi_primes <omp_enabled> <omp_nubmer_of_thread> <id_algorithm> <offset> <limit>" << endl;
		cout << "<omp_enabled>: 1 use multithread with openMP, 0 no" << endl;
		cout << "<omp_numer_of_thread>: the number of threads to use" << endl;
		cout << "<id_algorithm>: the id of the method choosen" << endl;
		cout << "<offset>: the number of gallery as the initial offset from which start the search" << endl;
		cout << "<limit>: the number of gallery as the upper limit where stop the search" << endl;
		cout << endl;
		cout << "Implemented algorithms:" << endl;
		cout << PrimesFinder::getAvailableAlgorithms() << endl;
		cout << endl;
		exit(0);
	}

	//Number of gallery = number of tasks
	initialGallery = atoi(argv[4]);
	lastGallery = atoi(argv[5]);
	nTask = lastGallery - initialGallery;

	//Init MPI
	MPI_Init(&argc, &argv);                                 //chiamata alla funzione MPI_int che informa il sistema operativo che si sta eseguendo un programma MPI
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);            	//funzione che restituisce il numero totale di processi allocati
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);              		//funzione che restituisce il numero del processo che l’ha chiamata.
	MPI_Get_processor_name(processor_name, &namelen);

	//Logger initialization
	string file = "twin_primes";
	logger.init(myid, getpid(), file);

	LOGGER << "Process " << myid << " of " << numprocs << " is on " << processor_name << "(PID " << getpid() << ")" << endl;

	nCPU = numprocs;

	unsigned long long command = (unsigned long long) initialGallery * PrimesFinder::sizeGallery;
	size_brun result = 0; //Partial Brun's constant received
	startwtime = MPI_Wtime();

	if (myid == 0) {
		//Init: send n-1 request to different process
		for (int i = 0; i < (nCPU - 1); i++) {
			destination = (i % (nCPU - 1)) + 1;
			LOGGER << "Sending offset " << command << " to process " << destination << endl;
			MPI_Ssend(&command, 1, MPI_UNSIGNED_LONG_LONG, destination, 0, MPI_COMM_WORLD);
			command += PrimesFinder::sizeGallery;
		}
		//Wait for the fastest process
		for (int i = 0; i < nTask - nCPU + 1; i++) {
			MPI_Recv(&result, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			LOGGER << "Received result " << result << ". Now sending offset " << command << " to process " << status.MPI_SOURCE << endl;
			updateBrun(result);
			MPI_Ssend(&command, 1, MPI_UNSIGNED_LONG_LONG, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
			command += PrimesFinder::sizeGallery;
		}

		command = 1;
		//ending for, i'm waiting for all the active processes and send the termination command (1)
		for (int i = 0; i < nCPU - 1; i++) {
			MPI_Recv(&result, 1, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			LOGGER << "Received " << result << " from " << status.MPI_SOURCE << ". Sending close command!" << endl;
			updateBrun(result);
			MPI_Send(&command, 1, MPI_UNSIGNED_LONG_LONG, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
		}
	} else { //Slaves
		//PrimesFinde initialization
		PrimesFinder p;
		p.setOpenMP(atoi(argv[1]));
		p.setNumOfThreads(atoi(argv[2]));
		p.setAlgorithm(atoi(argv[3]));

		do {
			MPI_Recv(&command, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			if (command != 1) {
				LOGGER << "Received offset " << command << ". Start find primes!" << endl;
				numberOfTask++;
				p.setOffset(command);
				result = p.find();
				MPI_Ssend(&result, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
			}
		} while (command != 1);
	}
	endwtime = MPI_Wtime();

	MPI_Barrier( MPI_COMM_WORLD);
	//Final statistic
	if (myid > 0) {
		LOGGER << "Number of gallery received: " << numberOfTask << ". Runtime: " << endwtime - startwtime << " s." << endl;
	} else {
		LOGGER << "Runtime: " << endwtime - startwtime << " s" << endl;
	}
	LOGGER << "Computation terminated!" << endl;
	LOGGER << "Final Brun's constant value: " << std::setprecision(std::numeric_limits<size_brun>::digits10 + 1) << brun << endl;

	//Chiusura della funzione che informa il sistema operativo di un programma MPI
	MPI_Finalize();
	return 0;
}
