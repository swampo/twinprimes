# TwinPrimes #

### What is it? ###
TwinPrimes is a program to compute primes numbers, twin primes and the Brun's constant
in a distributed way using MPI and/or OpenMP.

The program find all primes by dividing the numbers in galleries: each gallery is assigned to a different process that will analyze it to find all the primes and twin primes.

This was used principally on a Raspberry Pi cluster, with Raspbian OS, but it should work on every system where MPI and OpenMP are availables.

### The Latest Version ###
Details of the latest version can be found on the TwinPrimes project page under
https://bitbucket.org/swampo/twinprimes.


## Outline ##
* Dependencies
* Implemented algorithms
* Command Line Arguments
* How to run with MPI
* Compile the program
* Logs
* Contacts


## Dependencies ##
This programs needs the followings softwares installed on the system.

### MPI ###
The version used is MPICH: http://www.mpich.org/downloads/

The same version must be installed on all the machines.
The executable of the program must be present over all the machines.

### NFS Server \ Client ###
This is not mandatory.

May be used to share the directory containing the executable of the program and the log files.
An example of installation can be found at:
http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/


## Implemented algorithms ##
The search of primes can be executed using different methods and some their optimization.

    0 - Basic Primality Test with trial division
    1 - Basic PT using preshooting
    2 - Basic PT searching only twins
    3 - Basic PT with preshooting + only twins
    4 - Randomized Primality Test
    5 - Randomized PT using preshooting
    6 - Randomized PT searching only twins
    7 - Randomized PT with preshooting + only twins
    8 - Rabin Miller Primality Test
    9 - Rabin Miller PT using preshooting
    10 - Rabin Miller PT searching only twins
    11 - Rabin Miller PT with preshooting + only twins
    12 - Sieve of Eratosthenes using char to store values (Not optimized)
    13 - Sieve of Eratosthenes using int to store values (Not optimized)
    14 - SoE (char) optimized: even numbers eliminated during initialization)
    15 - SoE (int) optimized: even numbers eliminated during initialization
    16 - SoE cache: try to exploit the cache considering subgalleries
    17 - SoE using preshooting (array of char)
    18 - SoE using preshooting compressed (array of bit)
    19 - SoE cache using preshooting
    20 - TEST SoE cache multiple tests to find best subgallery value
    21 - TEST SoE cache using preshooting multiple tests to find best subgallery value


## Command Line Arguments ##
Example:  ./mpi_primes <omp_enabled> <omp_number_of_thread> <id_algorithm> <offset> <limit>

    - <omp_enabled>
      1 use multithread with openMP, 0 don't use multithread
    - <omp_number_of_thread>
      the number of threads to use
    - <id_algorithm>
      the id of the method choosen
    - <offset>
      the number of gallery as the initial offset from which start the search
    - <limit>
      the number of gallery as the upper limit where stop the search


## How to run with MPI##
To launch different jobs over the same machine or different machines, mpirun must be used.

Example:  mpirun -np <np> [-hosts host1,host2,host3,..,hostN] ./mpi_primes omp_enabled omp_number_of_thread id_algorithm offset limit

    -np <np>
     The number of process (at least 2)
    -hosts
     The list of nodes. Accept also the IP of nodes

### Notes: ###
At least 2 process are required.
The first is the master, that distributes the galleries to the slaves and collects the final results.

mpirun create the processes over the different nodes in a cyclic way: first job to the first node, second job to the second node, and so on.


## Compile the program ##
To compile the program with the support to MPI and OpenMP use:

mpic++ mpi_primes.cpp -o mpi_primes -std=c++11 -O3 -fopenmp


## Logs ##
TwinPrimes implements a Logger that saves into different logs, one for each process, all informations about the initialization, the gallery analyzed and the final results.

The files are named as *twin_primes_X.log*, where X is the ID of the process.
The logs are saved in the same folder of the executable.


## Contacts ##
Project for the course of **Advanced Algorithms and Parallel Programming** - Prof. F. Ferrandi - A.A 2015-2016 (Politecnico di Milano).


### Developed by: ###
* Fabiano Riccardi - fabiano.riccardi@mail.polimi.it
* Andrea Salmoiraghi - andrea1.salmoiraghi@mail.polimi.it, andrea.salmoiraghi@gmail.com