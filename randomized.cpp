/***************************************************************************
 *   Copyright (C) 2016 by Fabiano Riccardi, Andrea Salmoiraghi            *
 *                                                                         *
 *   This file is part of TwinPrimes.                                      *
 *                                                                         *
 *   TwinPrimes is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   TwinPrimes is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Twin Primes; if not, see <http://www.gnu.org/licenses/>    *
 ***************************************************************************/

/*
 * Primality test with randomized algorithm seen during the lessons
 * Check a given number, return true if probably prime
 */

static unsigned long long power(unsigned long long a, unsigned long long p, unsigned long long n, bool& isProbablyPrime) {
	/* computes a^p mod n and checks during the
	computation whether there is an x with x^2 modn = 1 and x!=1,n-1 */
	if (p == 0)
		return 1;
	unsigned long long x = power(a, p / 2, n, isProbablyPrime);
	unsigned long long result = (x * x) % n;
	/* check whether x^2 mod n = 1 and x!=1,n-1 */
	if (result == 1 && x != 1 && x != n - 1 )
		isProbablyPrime = false;
	if (p % 2 == 1)
		result = (a * result) % n;
	return result;
}

static bool checkRandIsPrime(unsigned long long n) {
	/* executes the randomized primality test for a
	  chosen at random*/
	srand(time(NULL));
	unsigned long long a = rand() % (n - 1) - 1 + 2;
	bool isProbablyPrime = true;
	unsigned long long result = power(a, n - 1, n, isProbablyPrime);
	if (result != 1 || !isProbablyPrime)
		return false; //not prime
	else
		return true; //probably prime
}

static bool randIsPrime(unsigned long long n, int repeats) {
	bool isProbablyPrime = true;
	for (int i = 0; i < repeats; i++) {
		isProbablyPrime = checkRandIsPrime(n);
		if (!isProbablyPrime) {
			break;
		}
	}
	return isProbablyPrime;
}