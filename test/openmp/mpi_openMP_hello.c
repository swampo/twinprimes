/* C Example */
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[])
{
  int nthreads, tid;

  int rank, size;
  MPI_Init (&argc, &argv);      /* starts MPI */
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);        /* get current process id */
  MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */
  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  /* Fork a team of threads giving them their own copies of variables */
  #pragma omp parallel private(nthreads, tid)
  {
    // Code inside this region runs in parallel.
    tid = omp_get_thread_num();
    printf( "Hello world! Process %d of %d on node %s, thread = %d\n", rank, size, processor_name, tid);

    /* Only master thread does this */
    if (tid == 0)
    {
      nthreads = omp_get_num_threads();
      printf("Number of threads = %d\n", nthreads);
    }/* All threads join master thread and disband */
  }
  MPI_Finalize();
  return 0;
}
