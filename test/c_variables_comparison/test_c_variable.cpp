#include <stdio.h>
#include <iostream>
#include "logger.c"
#include <chrono>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;


int main(){
	chrono::high_resolution_clock::time_point start;
  	chrono::high_resolution_clock::time_point stop;

	Logger logger(0,0,"result.txt");

	for(int max = 100000; max < 1000000001; max *= 10){
		cout << "Test eseguiti con # cicli=" <<max<<endl;

		//NB i++ richiede lo stesso tempo di una somma arbitraria
		int k=0;
		start = chrono::high_resolution_clock::now();
		for(unsigned int i=0;i<max;i++){
			k=k+2;
		}
		stop = chrono::high_resolution_clock::now();
		cout << "Tempo per somme d'interi " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;

		start = chrono::high_resolution_clock::now();
		unsigned long long j=0;
		for(unsigned int i=0;i<max;i++){
			j=j+2;
		}
		stop = chrono::high_resolution_clock::now();
		cout << "Tempo per somme di llu " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;


		start = chrono::high_resolution_clock::now();
		unsigned int mod=0;
		for(unsigned int i=1;i<=max;i++){
			mod=mod%i;
		}
		stop = chrono::high_resolution_clock::now();
		cout << "Tempo per moduli di u int " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;

		start = chrono::high_resolution_clock::now();
		mod=0;
		for(unsigned long long i=1;i<=max;i++){
			mod=mod%i;
		}
		stop = chrono::high_resolution_clock::now();
		cout << "Tempo per moduli di llu " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;

		cout<<endl<<endl;
	}
}