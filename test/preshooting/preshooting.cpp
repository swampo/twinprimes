/***************************************************************************
 *   Copyright (C) 2016 by Fabiano Riccardi, Andrea Salmoiraghi            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <http://www.gnu.org/licenses/>   *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <bitset>
#include <string.h>


using namespace std;

static const char* fileStats = "preshooting_stats.txt";

//Default gallery size 2*3*5*7*11*13*17*19*23=223092870
static const int size = 223092870;
static const char* fileBitset = "preshooting_bitset.txt";
static const char* fileChar = "preshooting_char.txt";
static const char* fileBool = "preshooting_bool.txt";
static const char* fileInt = "preshooting_int.txt";

//Test different type
bitset<size/30*3> patternBitset;
bool patternBoolArr[size/30*3] = {};
char patternCharArr[size/30*3] = {};
int patternIntArr[size/30*3] = {};
vector<bool> patternBoolVec (size/30*3, 0);
vector<char> patternCharVec (size/30*3, 0);
vector<int> patternIntVec (size/30*3, 0);

//In modulo 30 twin primes can be only 11,17,29
unsigned int potentialPrimesList[] = {11,17,29};
vector<unsigned int> potentialPrimes(potentialPrimesList, potentialPrimesList + sizeof(potentialPrimesList) / sizeof(int) );

void createBitset(){
  int i, k;
  int n;
  int base;

  i=0;
  k=0;
  base = 0;
  for(n=base+potentialPrimes[k]; n<size; n=base+potentialPrimes[k]){
    //In modulo 30 we have already eliminated 2,3,5 multiples
    if(!(n%7==0 || n%11==0 || n%13==0 || n%17==0 || n%19==0 || n%23==0)){
      patternBitset.set(i, true);
    }
    i++;
    k++;
    if(k==3){
      base += 30;
      k = 0;
    }
  }
}

template <typename T, size_t N>
void createArr(T (&arr)[N]){
  int i, k;
  int n;
  int base;

  i=0;
  k=0;
  base = 0;
  for(n=base+potentialPrimes[k]; n<size; n=base+potentialPrimes[k]){
    //In modulo 30 we have already eliminated 2,3,5 multiples
    if(!(n%7==0 || n%11==0 || n%13==0 || n%17==0 || n%19==0 || n%23==0)){
      arr[i] = 1;
    }
    i++;
    k++;
    if(k==3){
      base += 30;
      k = 0;
    }
  }
}

template <typename T>
void createVec(vector<T> &vector){
  int i, k;
  int n;
  int base;

  i=0;
  k=0;
  base = 0;
  for(n=base+potentialPrimes[k]; n<size; n=base+potentialPrimes[k]){
    //In modulo 30 we have already eliminated 2,3,5 multiples
    if(!(n%7==0 || n%11==0 || n%13==0 || n%17==0 || n%19==0 || n%23==0)){
      vector[i] = 1;
    }
    i++;
    k++;
    if(k==3){
      base += 30;
      k = 0;
    }
  }

  // Verify
  // for(i = 0; i< 20; i++){
  //   cout << (i/3)*30+potentialPrimes[i%3] << " - ";
  //   printf("%x\n",vector[i]);
  // }
}

int writeBitset(){
  ofstream file(fileBitset, ios::out | ios::binary);
  if(!file.is_open()){
    cout << "Error during opening of file!" << endl;
    exit(0);
  }
  int nbytes = (double)patternBitset.size()/8 + 0.5;
  for(int i=0; i<nbytes; i++){
    char byte = 0;
    for(int k=0; k<8; k++){
      if(i*8+k>=patternBitset.size()){
        break;
      }
      byte |= patternBitset[i*8+k]<<k;
    }
    file.write(&byte,1);
  }
  file.close();
  return nbytes;
}

template <typename T, size_t N>
int writeArr(T (&arr)[N], const char* name){
  ofstream file(name, ios::out | ios::binary);
  if(!file.is_open()){
    cout << "Error during opening of file!" << endl;
    exit(0);
  }
  file.write((const char*)arr,sizeof(arr));
  file.close();

  // Verify
  // for(int i=0;i<10;i++){
  //   cout << (i/3)*30+potentialPrimes[i%3] << " - ";
  //   printf("%x\n",arr[i]);
  // }

  return sizeof(arr);
}

void readBitset(){
  streampos fsize;
  char byte;
  int i;
  ifstream file(fileBitset, ios::ate | ios::in | ios::binary);
  if(!file.is_open()){
    cout << "Error during opening of file!" << endl;
    exit(0);
  }
  fsize = file.tellg();
  cout << "File size: " << fsize << endl;
  file.seekg (0, ios::beg);
  i = 0;
  while(!file.eof()){
    file.read(&byte,1);
    // Verify
    // if(i<10){
    //   bitset<8> x(byte);
    //   cout << "Verify " << i << ": " << x << endl;
    // }
    for(int k=0; k<8; k++){
      if(i*8+k>=patternBitset.size()){
        break;
      }
      patternBitset.set(i*8+k, (byte >> k) & 1);
    }
    i++;
  }
  file.close();

  // Verify
  // for(i=0;i<10;i++){
  //   cout << (i/3)*30+potentialPrimes[i%3] << " - ";
  //   cout << patternBitset[i] << endl;
  // }
}

template <typename T, size_t N>
void readArr(T (&arr)[N], const char* name){
  streampos fsize;
  ifstream file(name, ios::ate | ios::in | ios::binary);
  if(!file.is_open()){
    cout << "Error during opening of file!" << endl;
    exit(0);
  }
  fsize = file.tellg();
  cout << "File size: " << fsize << endl;
  file.seekg (0, ios::beg);
  file.read((char*)arr, sizeof(arr));
  file.close();

  // Verify
  // for(int i=0;i<10;i++){
  //   cout << (i/3)*30+potentialPrimes[i%3] << " - ";
  //   printf("%x\n",arr[i]);
  // }
}


template <typename T>
void readVec(vector<T> &vector, const char* name){
  streampos fsize;
  ifstream file(name, ios::ate | ios::in | ios::binary);
  if(!file.is_open()){
    cout << "Error during opening of file!" << endl;
    exit(0);
  }
  fsize = file.tellg();
  cout << "File size: " << fsize << endl;
  file.seekg (0, ios::beg);
  file.read((char *)vector.data(), size/30*3 * sizeof(T));
  file.close();

  // Verify
  // for(int i=0;i<10;i++){
  //   cout << (i/3)*30+potentialPrimes[i%3] << " - ";
  //   printf("%x\n",vector[i]);
  // }
}

int testRandomBitset(int max){
  int n = 0;
  srand (time(NULL));
  for(int i=0; i<max; i++){
    int k = rand() % (size/30*3);
    if(patternBitset[k]){
      n++;
    }
  }
  return n;
}

template <typename T, size_t N>
int testRandomArr(T (&arr)[N], int max){
  int n = 0;
  srand (time(NULL));
  for(int i=0; i<max; i++){
    int k = rand() % (size/30*3);
    if(arr[k]){
      n++;
    }
  }
  return n;
}

template <typename T>
int testRandomVec(vector<T> &vector, int max){
  int n = 0;
  srand (time(NULL));
  for(int i=0; i<max; i++){
    int k = rand() % (size/30*3);
    if(vector[k]){
      n++;
    }
  }
  return n;
}

int testSequentialBitset(int max){
  int n = 0;
  int k = 0;
  for(int i=0; i<max; i++){
    if(patternBitset[k]){
      n++;
    }
    k++;
    if(k>=size/30*3){
      k=0;
    }
  }
  return n;
}

template <typename T, size_t N>
int testSequentialArr(T (&arr)[N], int max){
  int n = 0;
  int k = 0;
  for(int i=0; i<max; i++){
    if(arr[k]){
      n++;
    }
    k++;
    if(k>=size/30*3){
      k=0;
    }
  }
  return n;
}

template <typename T>
int testSequentialVec(vector<T> &vector, int max){
  int n = 0;
  int k = 0;
  for(int i=0; i<max; i++){
    if(vector[k]){
      n++;
    }
    k++;
    if(k>=size/30*3){
      k=0;
    }
  }
  return n;
}

int main(int argc,char *argv[]){
  ofstream stats(fileStats, ios::out);
  if(!stats.is_open()){
    cout << "Error opening file for statistics..." << endl;
    exit(0);
  }

  chrono::high_resolution_clock::time_point start;
  chrono::high_resolution_clock::time_point stop;

  int max;

  cout << "Gallery size: " << size << endl << endl;
  stats << "Gallery size: " << size << endl << endl;

  /*************************************************************
   * CREATE
   *************************************************************/
  stats << "TESTS CREATE POTENTIAL PRIMES" << endl;
  stats << "Type\t\tTime (ms)\tSize (byte)" << endl;

  cout << "TEST CREATE BITSET" << endl;
  start = chrono::high_resolution_clock::now();
  createBitset();
  stop = chrono::high_resolution_clock::now();
  cout << "Time:" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << sizeof(patternBitset) << endl;
  stats << "bitset";
  stats << "\t\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << sizeof(patternBitset) << endl;

  cout << endl;

  cout << "TEST CREATE BOOL ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  createArr(patternBoolArr);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << sizeof(patternBoolArr) << endl;
  stats << "bool array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << sizeof(patternBoolArr) << endl;

  cout << endl;

  cout << "TEST CREATE CHAR ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  createArr(patternCharArr);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << sizeof(patternCharArr) << endl;
  stats << "char array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << sizeof(patternCharArr) << endl;

  cout << endl;

  cout << "TEST CREATE INT ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  createArr(patternIntArr);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << sizeof(patternIntArr) << endl;
  stats << "int array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << sizeof(patternIntArr) << endl;

  cout << endl;

  cout << "TEST CREATE BOOL VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  createVec(patternBoolVec);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << patternCharVec.size()*sizeof(bool) << endl;
  stats << "bool vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << patternCharVec.size()*sizeof(bool) << endl;

  cout << endl;

  cout << "TEST CREATE CHAR VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  createVec(patternCharVec);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << patternCharVec.size()*sizeof(char) << endl;
  stats << "char vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << patternCharVec.size()*sizeof(char) << endl;

  cout << endl;

  cout << "TEST CREATE INT VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  createVec(patternIntVec);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Size: " << patternCharVec.size()*sizeof(int) << endl;
  stats << "int vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << patternCharVec.size()*sizeof(int) << endl;

  cout << endl;

  /*************************************************************
   * WRITE
   *************************************************************/
  stats << endl << "TESTS WRITE FILE" << endl;
  stats << "Type\t\tTime (ms)\tSize (byte)" << endl;

  int nbytes;
  cout << "TEST WRITE BITSET" << endl;
  start = chrono::high_resolution_clock::now();
  nbytes = writeBitset();
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Number of bytes written: " << nbytes << endl;
  stats << "bitset";
  stats << "\t\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << nbytes << endl;

  cout << endl;

  cout << "TEST WRITE BOOL ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  nbytes = writeArr(patternBoolArr, fileBool);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Number of bytes written: " << nbytes << endl;
  stats << "bool array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << nbytes << endl;

  cout << endl;

  cout << "TEST WRITE CHAR ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  nbytes = writeArr(patternCharArr, fileChar);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Number of bytes written: " << nbytes << endl;
  stats << "char array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << nbytes << endl;

  cout << endl;

  cout << "TEST WRITE INT ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  nbytes = writeArr(patternIntArr, fileInt);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  cout << "Number of bytes written: " << nbytes << endl;
  stats << "int array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count();
  stats << "\t" << nbytes << endl;

  cout << endl;

  /*************************************************************
   * READ
   *************************************************************/
  stats << endl << "TESTS READ FILE" << endl;
  stats << "Type\t\tTime (ms)" << endl;

  cout << "TEST READ BITSET" << endl;
  start = chrono::high_resolution_clock::now();
  readBitset();
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "bitset";
  stats << "\t\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ BOOL ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  readArr(patternBoolArr, fileBool);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "bool array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ CHAR ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  readArr(patternCharArr, fileChar);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "char array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ INT ARRAY" << endl;
  start = chrono::high_resolution_clock::now();
  readArr(patternIntArr, fileInt);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "int array";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ BOOL VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  // readVec(patternBoolVec, fileBool);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "bool vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ CHAR VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  readVec(patternCharVec, fileChar);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "char vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  cout << "TEST READ INT VECTOR" << endl;
  start = chrono::high_resolution_clock::now();
  readVec(patternIntVec, fileInt);
  stop = chrono::high_resolution_clock::now();
  cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << " ms" << endl;
  stats << "int vector";
  stats << "\t" << chrono::duration_cast<chrono::milliseconds>(stop-start).count() << endl;

  cout << endl;

  /*************************************************************
   * RANDOM AND SEQUENTIAL ACCESS
   *************************************************************/
  stats << endl << "TESTS ACCESS" << endl;
  stats << "Type\t\tNumber\t\tRandom (ms)\tSequential (ms)" << endl;

  int n;
  chrono::milliseconds randomTime;
  chrono::milliseconds sequentialTime;
  for(int max = 100000; max < 1000000001; max *= 10){
    cout << "Number of access: " << max << endl << endl;

    cout << "TEST ACCESS BITSET" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomBitset(max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialBitset(max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "bitset";
    stats << "\t\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS BOOL ARR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomArr(patternBoolArr,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialArr(patternBoolArr,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "bool array";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS CHAR ARR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomArr(patternCharArr,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialArr(patternCharArr,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "char array";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS INT ARR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomArr(patternIntArr,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialArr(patternIntArr,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "int array";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS BOOL VECTOR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomVec(patternBoolVec,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialVec(patternBoolVec,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "bool vector";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS CHAR VECTOR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomVec(patternCharVec,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialVec(patternCharVec,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "char vector";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << "TEST ACCESS INT VECTOR" << endl;
    start = chrono::high_resolution_clock::now();
    n = testRandomVec(patternIntVec,max);
    stop = chrono::high_resolution_clock::now();
    randomTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    start = chrono::high_resolution_clock::now();
    n = testSequentialVec(patternIntVec,max);
    stop = chrono::high_resolution_clock::now();
    sequentialTime = chrono::duration_cast<chrono::milliseconds>(stop-start);
    cout << "Potential primes found: " << n << endl;
    cout << "Time: ";
    cout << "Random: " << randomTime.count() << " ms" << " - Sequential: " << sequentialTime.count() << " ms" << endl;
    stats << "int vector";
    stats << "\t" << max;
    stats << "\t\t" << randomTime.count();
    stats << "\t" << sequentialTime.count() << endl;

    cout << endl;
  }

  return 0;
}
