#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool isPrime(long n) {
    long i;

    for(i=2; i<=n/2; i++) {
        if(n%i==0) return false;
    }
    return true;
}

int main(int argc, char *argv[]) {
  int rank, size, len;
  char name[MPI_MAX_PROCESSOR_NAME];
  int long last,i, num_primes=0;

  last = atol(argv[1]);

  MPI_Init(&argc, &argv);
  
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(name, &len);

  int long chunk = last/size;
  int long rest = last%size;
  
  for(i=rank; i<last; i+=size) {
      if(isPrime(i)) num_primes++;
  }
  printf("Total number of primes = %d\n", num_primes);

  MPI_Finalize();
  return 0;
}
